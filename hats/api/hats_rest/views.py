from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Hat, LocationVO


# Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "id",
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ['id', 'style_name', 'fabric', 'color', 'hat_url']

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ['id', 'style_name', 'fabric', 'color', 'hat_url', 'location']

    encoders = {"location": LocationVOEncoder()}


@require_http_methods(["GET", "POST"])
def list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
            return JsonResponse(
                {"hats": hats},
                encoder=HatListEncoder,
            )
    else:
        content = json.loads(request.body)
        # print(content)

        try:
            location_href = content['location']
            location = LocationVO.objects.get(
                import_href=f"/api/locations/{location_href}/")
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Location not found, invalid location id"},
                status=400,
            )
        hats = Hat.objects.create(**content)

        return JsonResponse(
            hats,
            encoder=HatListEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def show_hat(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
            safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Hat.objects.filter(id=id).update(**content)

        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )
    else:
        try:
            count, _ = Hat.objects.filter(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0}
            )
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "That hat does not exist you silly goose."},
                status=404,
                safe=False
            )
