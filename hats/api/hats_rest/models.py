from django.core.exceptions import ObjectDoesNotExist
from django.db import models

# Create your models here.


class LocationVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.CharField(max_length=100)
    shelf_number = models.CharField(max_length=100)

    # objects = models.Manager()

    def __str__(self):
        return f"{self.closet_name} {self.section_number} {self.shelf_number}"


class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    hat_url = models.URLField(max_length=200)

    location = models.ForeignKey(
        LocationVO,
        related_name='hats',
        on_delete=models.CASCADE
    )
