from django.contrib import admin
from .models import Hat, LocationVO

# Register your models here.


@admin.register(Hat)
class Hatadmin(admin.ModelAdmin):
    pass


@admin.register(LocationVO)
class LocationVOadmin(admin.ModelAdmin):
    pass
