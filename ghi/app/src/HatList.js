import React, { useState, useEffect } from 'react';


function HatList() {
  const [hats, setHats] = useState([]);

  const fetchHat = async () => {
    const response = await fetch('http://localhost:8090/api/hats/');

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  };

  useEffect(() => {
    fetchHat();
  }, []);


  const handleDelete = async (e) => {
    console.log(e);

    const url = `http://localhost:8090/api/hats/${e.target.id}/`;
    const fetchConfigs = {
      method: 'Delete',
      headers: {
        "Content-Type": "application/json",
      }
    };

    const response = await fetch(url, fetchConfigs);
    const data = await response.json();

    setHats(hats.filter((hat) => String(hat.id) !== e.target.id));


  };


  // function handleDelete(e) {
  //   console.log();
  //   const url = `http://localhost:8090/api/hats/${id}/`;
  //   const fetchConfig = { method: 'DELETE' };
  //   const response = fetch(url, fetchConfig);

  //   setHats(hats.filter(function (hat) {
  //     return hat.id !== id;
  //   }
  //   ));

  // };


  return (
    <div>
      <div className="px-4 py-5 my-5 mt-0 text-center">

        <h1 className="display-5 fw-bold">My Collection</h1>
        <div className="col-lg-6 mx-auto">
          <p> Bro... sick hats bro.</p>
          <div className="d-grid gap-4 d-sm-flex justify-content-sm-center">
            <a href="/hats/new" className="btn btn-info btn-lg px-4 gap-3">Add A Hat</a>
          </div>
        </div>

        <div className="row">

          <table className='table table-hover'>
            <thead>
              <tr>
                <th>Fabric</th>
                <th>Style</th>
                <th>Color</th>
                <th>Location</th>
                <th>Photo</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {hats.map(hat => {
                return (
                  <tr key={hat.id}>
                    <td>{hat.fabric}</td>
                    <td>{hat.style_name}</td>
                    <td>{hat.color}</td>
                    <td>{hat.location}</td>
                    <td>
                      <img src={hat.hat_url} alt='a hat' width='50' height='50' />
                    </td>
                    <td><button onClick={handleDelete} id={hat.id} className="btn btn-danger">delete</button></td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );

};

export default HatList;
