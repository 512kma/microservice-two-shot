import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import LocationForm from './LocationForm';
import HatForm from './HatForm';
import HatList from './HatList';






function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats/" element={<HatList />} />
          <Route path="hats/new" element={<HatForm />} />
          <Route path="locations/" element={<LocationForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
