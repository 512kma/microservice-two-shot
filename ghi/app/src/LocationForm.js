import React, { useState } from 'react';


function LocationForm(props) {

  const [closetName, setClosetName] = useState("");
  const [sectionNumber, setSectionNumber] = useState("");
  const [shelfNumber, setShelfNumber] = useState("");


  const handleNameChange = e => {
    const value = e.target.value;
    setClosetName(value);
  };

  const handleSectionChange = e => {
    const value = e.target.value;
    setSectionNumber(value);
  };

  const handleShelfChange = e => {
    const value = e.target.value;
    setShelfNumber(value);
  };

  const handleSubmit = async e => {
    e.preventDefault();

    const data = {};
    data.closet_name = closetName;
    data.section_number = sectionNumber;
    data.shelf_number = shelfNumber;

    const locationUrl = "http://localhost:8100/api/locations/";
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json"
      }
    };

    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      const newLocation = await response.json();

      setClosetName("");
      setSectionNumber("");
      setShelfNumber("");

    }
  };


  return (
    <div className='row'>
      <div className='offset-3 col-6'>
        <div className='shadow p-4 mt-4'>
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id='create-location-form'>
            <div className='form-floating mb-3'>
              <input
                onChange={handleNameChange}
                value={closetName}
                placeholder='Closet Name'
                required
                type='text'
                name='name'
                id='name'
                className='form-control'
              />
              <label htmlFor='name'>Closet Name</label>
            </div>
            <div className='form-floating mb-3'>
              <input
                onChange={handleSectionChange}
                value={sectionNumber}
                placeholder='Section Number'
                required
                type='number'
                name='section_number'
                id='section_number'
                className='form-control'
              />
              <label htmlFor='section_number'>Section Number</label>
            </div>
            <div className='form-floating mb-3'>
              <input
                onChange={handleShelfChange}
                value={shelfNumber}
                placeholder='Shelf Number'
                required
                type='number'
                name='shelf_number'
                id='shelf_number'
                className='form-control'
              />
              <label htmlFor='shelf_number'>Shelf Number</label>
            </div>

            <button className='btn btn-primary'>Create</button>
          </form>

        </div>
      </div>
    </div>
  );
}

export default LocationForm;
