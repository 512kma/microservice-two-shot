import React, { useEffect, useState } from "react";

function HatForm() {

  const [locations, setLocations] = useState([]);
  const [fabric, setFabric] = useState('');
  const [style, setStyleName] = useState('');
  const [color, setColor] = useState('');

  const [location, setLocation] = useState('');


  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.fabric = fabric;
    data.style_name = style;
    data.color = color;
    data.location = location;

    const hatsUrl = "http://localhost:8090/api/hats/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      }
    };

    const response = await fetch(hatsUrl, fetchConfig);
    // console.log(data);
    // console.log(response);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);
      setFabric('');
      setStyleName('');
      setColor('');
      setLocation('');
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8100/api/locations/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  const handleFabricChange = event => {
    const value = event.target.value;
    setFabric(value);
  };
  const handleStyleChange = event => {
    const value = event.target.value;
    setStyleName(value);
  };
  const handleColorChange = event => {
    const value = event.target.value;
    setColor(value);
  };
  const handleLocationChange = event => {
    const value = event.target.value;
    setLocation(value);
  };


  useEffect(() => {
    fetchData();
  }, []);


  return (
    <div>
      <div className='offset-3 col-6'>
        <div className='shadow p-4 mt-4'>
          <h1>Add A Hat to Your Closet</h1>
          <form onSubmit={handleSubmit}>
            <div className='form-floating mb-3'>
              <input
                onChange={handleFabricChange}
                value={fabric}
                placeholder='Fabric'
                required
                type='text'
                name='fabric'
                id='fabric'
                className='form-control'
              />
              <label htmlFor='fabric'>Fabric</label>
            </div>
            <div className='form-floating mb-3'>
              <input
                onChange={handleStyleChange}
                value={style}
                placeholder='Style_name'
                required
                type='text'
                name='style_name'
                id='style_name'
                className='form-control'
              />
              <label htmlFor='style_name'>Style Name</label>
            </div>
            <div className='form-floating mb-3'>
              <input
                onChange={handleColorChange}
                value={color}
                placeholder='Color'
                required
                type='text'
                name='color'
                id='color'
                className='form-control'
              />
              <label htmlFor='color'>Color</label>
            </div>
            <div className='mb-3'>
              <label htmlFor='pic_url' className='form-label'>
                Pic Url
              </label>
              <textarea
                // onChange={handleUrlChange}
                // value={url}
                required
                type='text'
                name='pic_url'
                id='pic_url'
                className='form-control'
                rows='3'
              ></textarea>
            </div>
            <div className='mb-3'>
              <select
                onChange={handleLocationChange}
                value={location}
                required
                name='location'
                id='location'
                className='form-select'
              >
                <option value=''>Choose a Location</option>

                {locations.map(location => {
                  return (
                    <option value={location.id} key={location.id}>
                      closet: {location.closet_name} ||
                      shelf #{location.shelf_number}
                    </option>
                  );
                })}
              </select>
            </div>
            {/* <button className='btn btn-info'>Back</button> */}
            <button onSubmit={handleSubmit} className='btn btn-info'>Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
